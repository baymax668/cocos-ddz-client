
export interface IServerConfig {
  host: string,
  port: number
}

/**
 * 服务器配置表
 */
export const socketServerConfig: IServerConfig = {
  host: 'localhost',
  port: 3000
}
import { socketServerConfig } from "../../config/ServerConfig";
import Store from "../../store/Store";
import NetMgr from "../NetManager/NetMgr";

const { ccclass, property } = cc._decorator;

@ccclass
export default class LoginMgr extends cc.Component {

    start() {
        // net init
        NetMgr.init(socketServerConfig)
    }

    /**
     * 登录按钮点击事件
     */
    async onLoginGuestBtnClickEvent() {
        console.log('onLoginGuestBtnClickEvent')
        // 登录
        let user = await NetMgr.loginByGuest()
        console.log('user:', user)
        // 全局保存用户信息
        Store.user = user
        // 连接socket...
        await NetMgr.connect(Store.user.id)
        // 场景跳转
        cc.director.loadScene('hall')
    }
}

import Store from "../../store/Store";
import NetMgr from "../NetManager/NetMgr";

const { ccclass, property } = cc._decorator;

@ccclass
export default class HallMgr extends cc.Component {
    start() {

    }

    /**
     * 快速开始按钮点击事件
     */
    async onQuickStartBtnClickEvent() {
        console.log('onQuickStartBtnClickEvent')
        // 请求加入房间
        const { room, index } = await NetMgr.joinRoom(Store.user)
        console.log('room:', room)
        // 保存房间数据
        Store.room = room
        Store.index = index
        // 切换场景
        cc.director.loadScene('room')
    }

}

import { IServerConfig } from "../../config/ServerConfig";
import { IUserVo } from "../../data/User";
import HttpUtil from "../../utils/HttpUtil";
import { io, Socket } from "socket.io-client";
import Store from "../../store/Store";
import { ResponseData } from "../../data/ResponseData";
import { IRoomVo } from "../../data/Room";
import { SocketRoomEvent } from "../../constant/SocketRoomEvent";
import { SocketGameEvent } from "../../constant/SocketGameEvent";
import { IPokerVo } from "../../data/Poker";

export default class NetMgr {
  private static socketConfig: IServerConfig
  private static socket: Socket

  /**
   * 初始化服务器配置信息
   * @param socketConfig 
   */
  public static init(socketConfig: IServerConfig) {
    this.socketConfig = socketConfig
    HttpUtil.baseUrl = 'http://localhost:3000/api/v1'
  }

  /**
   * socket连接
   * @param uid 
   * @returns 
   */
  public static connect(uid: string): Promise<void> {
    return new Promise((resolve, reject) => {

      this.socket = io(`ws://${this.socketConfig.host}:${this.socketConfig.port}`, {
        auth: {
          uid
        }
      });

      // 连接成功
      this.socket.on('connect', () => {
        resolve()
      })

      // 连接失败
      this.socket.on('connect_error', () => {
        // 断开连接
        this.socket.disconnect()
        reject()
      })

      // 断开连接
      this.socket.on('disconnect', () => {
        // 断开连接
        console.log('[socket] disconnect')
      })

    })
  }

  /**
   * 匿名登录API
   * @returns 
   */
  public static async loginByGuest(): Promise<IUserVo> {
    const result: ResponseData = (await HttpUtil.post('/user/login/guest')) as ResponseData
    const user = result.data.user as IUserVo
    return user
  }

  /**
   * 加入房间API
   */
  public static async joinRoom(user: IUserVo) {
    const result: ResponseData = (await HttpUtil.post('/room/join', { user })) as ResponseData
    const room: IRoomVo = result.data.room
    const index: number = result.data.index
    //const index: number = result.data.index // 座位
    return { room, index }
  }

  /**
   * 离开房间API
   */
  public static async leaveRoom(uid: string, roomId: string) {
    const result: ResponseData = (await HttpUtil.post('/room/leave', { uid, roomId })) as ResponseData
  }

  /**
   * 准备API
   */
  public static async prepare(uid: string, roomId: string, isPrepare: boolean) {
    const result: ResponseData = (await HttpUtil.post('/game/prepare', { uid, roomId, isPrepare })) as ResponseData
  }

  /**
   * 准备就绪API（客户端整理好牌后发生给服务端）
   */
  public static async ready(uid: string, roomId: string) {
    const result: ResponseData = (await HttpUtil.post('/game/ready', { uid, roomId })) as ResponseData
  }

  /**
   * 叫地主API
   */
  public static async call(uid: string, roomId: string, isCall: boolean) {
    const result: ResponseData = (await HttpUtil.post('/game/call', { uid, roomId, isCall })) as ResponseData
  }

  /**
   * 出牌API
   */
  public static async play(uid: string, roomId: string, isPlay: boolean, cards: Array<IPokerVo> = []) {
    const result: ResponseData = (await HttpUtil.post('/game/play', { uid, roomId, isPlay, cards })) as ResponseData
  }

  /**
   * 事件监听
   * @param event 
   * @param callback 
   */
  public static on(event: SocketRoomEvent | SocketGameEvent, callback: (data) => void) {
    this.socket.on(event, callback)
  }

  public static off(event: SocketRoomEvent | SocketGameEvent, fn?: (data) => void) {
    this.socket.off(event, fn)
  }
}
/**
 * 卡牌的牌型
 */
export enum PokerHand {
  T_A = 'T_A',                  // 单张
  T_AA = 'T_AA',                // 对子
  T_AAA = 'T_AAA',              // 三张
  T_AAA_B = 'T_AAA_B',          // 三带一
  T_AAA_BB = 'T_AAA_BB',        // 三带二、三带两对，如：AAA-BC、AAA-BBCC
  T_ABCDE = 'T_ABCDE',          // 顺子，>=5张连续的牌
  T_AABBCC = 'T_AABBCC',        // 双顺子，>=6 && len%2=0且两两连续的牌
  T_AAABBB = 'T_AAABBB',        // 三顺子（飞机），>=6 && len%3=0且三三连续的牌
  T_AAABBB_CD = 'T_AAABBB_CD',  // 飞机带翅膀，如：AAABBB-CD、AAABBB-CCDD、AAABBBCCC-DEF、AAABBBCCC-DDEEFF
  T_AAAA = 'T_AAAA',            // 炸弹
  T_ROCKET = 'T_ROCKET',         // 火箭（王炸）
  NONE = 'NONE'
}

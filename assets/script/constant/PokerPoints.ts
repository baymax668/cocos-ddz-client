/**
 * 卡牌点数
 * 用于规范点数常量
 */
export enum PokerPoints {
  P_NONE = 0,
  P_A = 1,
  P_2 = 2,
  P_3 = 3,
  P_4 = 4,
  P_5 = 5,
  P_6 = 6,
  P_7 = 7,
  P_8 = 8,
  P_9 = 9,
  P_10 = 10,
  P_J = 11,    // 王子
  P_Q = 12,    // 皇后
  P_K = 13,    // 国王
  P_BJ = 20,   // 小王 black joker
  P_RJ = 21,   // 大王 red joker
}


/**
 * 游戏相关操作枚举，用于显示房间不同状态下的操作按钮
 */
export enum GameOption {
  PREPARE = 0,    // 准备操作
  CALL = 1,       // 叫地主操作
  PALY = 2,       // 出牌操作
}
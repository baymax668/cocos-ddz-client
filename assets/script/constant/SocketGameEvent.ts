/**
 * 游戏相关的Socket事件
 */
export enum SocketGameEvent {
  CHANGE_STAGE = 'ctx-game-stage',        // 修改游戏阶段
  PREPARE = 'ctx-game-prepare',           // 有人准备/取消准备
  DEAL = 'ctx-game-deal',                 // 系统发牌
  SET_CALL_PLAYER = 'ctx-game-set-call-player',         // 系统通知轮到谁叫地主
  CALL = 'ctx-game-call',                 // 有人叫地主/不叫地主
  SET_LANDLORD = 'ctx-game-set-landlord', // 系统设置地主
  SET_PLAY = 'ctx-game-set-play',         // 系统通知轮到谁出牌
  PLAY_CARDS = 'ctx-game-play-cards',     // 有人出牌/不出牌
  RESULT = 'ctx-game-result',             // 游戏结果
}
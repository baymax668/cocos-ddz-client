/**
 * 卡牌花色
 */
export enum PokerSuits {
  SPADE = 3,    // 黑桃♠
  HEARD = 2,    // 红桃♥
  CLUB = 1,     // 梅花♣
  DIAMOND = 0,  // 方块♦
  NONE = 100,    // 没有花色（大小王）
}
/**
 * 游戏阶段
 */
export enum GameStage {
  PREPARATION = 0,        // 准备阶段
  REFEREE_DEAL = 1,       // 裁判发牌阶段
  CALL_LANDLORD = 2,      // 叫地主阶段
  GAMING = 3,             // 游戏中（玩家轮流出牌）
  SETTLEMENT = 4,         // 结算中
}
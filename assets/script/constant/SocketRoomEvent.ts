
/**
 * 房间相关的Socket事件
 */
export enum SocketRoomEvent {
  JOIN = 'ctx-room-join',                     // 有人加入
  LEAVE = 'ctx-room-leave',                   // 有人离开
}
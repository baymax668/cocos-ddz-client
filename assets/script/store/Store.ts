import { IRoomVo } from "../data/Room"
import { IUserVo } from "../data/User"

/**
 * 全局数据
 */
export default class Store {
    // 用户数据
    public static user: IUserVo
    // 房间数据
    public static room: IRoomVo
    public static index: number  // 本机玩家在房间的座位号
    private constructor() { }
}
/**
 * 用户数据对象接口
 */
export interface IUserVo {
  id: string
  name: string
  balance: number
}

/**
 * 用户对象
 */
// export class User implements IUserVo {
//   id: string
//   name: string
//   balance: number
// }
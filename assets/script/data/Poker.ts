import { PokerPoints } from "../constant/PokerPoints";
import { PokerSuits } from "../constant/PokerSuits";

/**
 * 扑克数据对象接口
 */
export interface IPokerVo {
  // 花色
  suit: PokerSuits
  // 点数
  point: PokerPoints
}

/**
 * 扑克牌对象
 * 思考/启发：将操作函数抽象到IPoker接口中，Poker类里的函数应该去实现IPoker接口，方便给不同规则的卡片游戏复用
 */
export class Poker implements IPokerVo {
  // 花色
  public suit: PokerSuits
  // 点数
  public point: PokerPoints

  constructor(poker: IPokerVo) {
    this.suit = poker.suit
    this.point = poker.point
  }

  /**
   * 点数的权重，用于比较大小 3~A=3~14、3~2=3~15
   */
  public get pointWeight() {
    let weight: number = this.point  // 权重
    // A和2的权重特殊处理
    switch (this.point) {
      case PokerPoints.P_A:
        weight = 14
        break;
      case PokerPoints.P_2:
        weight = 15
        break;
    }
    return weight
  }

  /**
   * 用于比较大小
   */
  public get value() {
    return this.suit + this.pointWeight * 100
  }

  /**
   * 根据点数的权重比较大小
   * @param poker 
   * @return 比较的差值 =0:相等；>0:比参数值大；<0:比参数值小
   */
  public compareByPointWeight(poker: IPokerVo): number {
    const pokerObj = new Poker(poker)
    return this.pointWeight - pokerObj.pointWeight
  }

  /**
   * 大小比较函数(包含点数和花色)
   * @param poker 
   */
  public compare(poker: IPokerVo): number {
    const pokerObj = new Poker(poker)
    return this.value - pokerObj.value
  }

  /**
   * 比较是否相等
   * @param poker 
   * @returns 
   */
  public isEquals(poker: IPokerVo): boolean {
    return this.suit === poker.suit && this.point === poker.point
  }

  public toString(): string {
    return `${this.suit}-${this.point}`
  }
}
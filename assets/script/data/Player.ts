import { Poker } from "./Poker";
import { IUserVo } from "./User";

export interface IPlayerVo extends IUserVo {
  prepare: boolean      // 是否准备
  cards: Array<Poker>   // 手牌
}

/**
 * 玩家对象
 */
export default class Player implements IPlayerVo {
  id: string;
  name: string;
  balance: number;
  // 是否准备
  public prepare: boolean = false
  // 手牌
  public cards: Array<Poker> = []

  constructor(player: IPlayerVo) {
    this.id = player.id
    this.name = player.name
    this.balance = player.balance
    this.prepare = player.prepare
    this.cards = player.cards
  }

  /**
   * 初始化手牌（系统发牌时使用）
   * @param cards 
   */
  public initCards(cards: Array<Poker>) {
    this.cards = cards
  }
}

/**
 * HTTP请求响应数据结构
 */
export interface ResponseData {
  code: number,
  message?: string,
  data?: any
}
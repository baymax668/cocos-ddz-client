import { GameStage } from "../constant/GameState"
import Player, { IPlayerVo } from "./Player"

/**
 * 房间数据对象接口
 */
export interface IRoomVo {
  id: string
  players: Array<IPlayerVo | undefined>
  maxOfPeople: number
}

/**
 * 房间对象
 */
export default class Room implements IRoomVo {
  public id: string             // 房间id
  public players: Player[]   // 房间玩家
  public state: GameStage       // 游戏阶段
  public maxOfPeople: number    // 房间最大人数

  constructor(room: IRoomVo) {
    this.resetData(room)
  }

  /**
   * 重置数据
   * @param room 
   */
  public resetData(room: IRoomVo) {
    this.id = room.id
    this.players = room.players.map((palyer => {
      if (palyer) {
        return new Player(palyer)
      } else {
        return undefined
      }
    }))
    this.maxOfPeople = room.maxOfPeople
    this.state = GameStage.PREPARATION
  }

  public getPlayerIndex(uid: string): number {
    return this.players.findIndex((player) => player.id === uid)
  }
}
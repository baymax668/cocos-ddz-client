/**
 * Socket数据对象（仅用于获取服务器的数据）
 */
export interface ISocketData {
  event: string       // 服务端触发的事件
  message?: number    // 消息说明
  data?: any          // 数据
}
/**
 * 随机函数工具
 */
export default class RandomUtil {
    // 工具类，不需要实例化
    private constructor() {
    }

    /**
     * 返回一个区间的整形随机数（不包含最大值）
     * @param min 最小值
     * @param max 最大值（不包含）
     * @returns 整形随机数
     */
    public static getRangeInt(min: number, max: number): number {
        if (max < min) {
            throw new Error('max < min')
        }
        min = Math.floor(min)
        max = Math.floor(max)

        return min + Math.floor(Math.random() * (max - min))
    }

}

import { IPokerVo, Poker } from "../data/Poker";
import CardCtrl from "./CardCtrl";

const { ccclass, property } = cc._decorator;

/**
 * 地主牌控制器
 * 会被挂载到LandCards预制体上
 */
@ccclass
export default class LandCardsCtrl extends cc.Component {

    @property({ type: cc.Node, multiline: true })
    cards: Array<cc.Node> = []; // 3张

    /**
     * 初始化地主牌
     * @param cards 
     */
    public init(cards: [IPokerVo, IPokerVo, IPokerVo]) {
        cards.forEach((card, index) => {
            const cardCtrl = this.cards[index].getComponent(CardCtrl)
            cardCtrl.init(new Poker(card))
            cardCtrl.touchEnable = false    // 关闭点击事件
        })
    }

    /**
     * 清空地主牌数据
     */
    public clear() {
        this.cards.forEach(card => {
            const cardCtrl = card.getComponent(CardCtrl)
            cardCtrl.init()
        })
    }
}

const { ccclass, property } = cc._decorator;

/**
 * CardNum预制体的控制类，控制卡牌上面显示的数字
 */
@ccclass
export default class CardNumCtrl extends cc.Component {
    @property(cc.Label)
    numLabel: cc.Label = null

    private _num: number = 0

    public init(num: number) {
        this.setNum(num)
    }

    public setNum(num: number) {
        this._num = num
        this.numLabel.string = num.toString()
    }

    /**
     * 减少显示的牌数
     * @param num 
     */
    public subtractNum(num: number) {
        this.setNum(this._num - num)
    }

    /**
     * 增加显示的牌数
     * @param num 
     */
    public increaseNum(num: number) {
        this.setNum(this._num + num)
    }
}

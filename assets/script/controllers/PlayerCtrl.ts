import { IUserVo } from "../data/User";

const { ccclass, property } = cc._decorator;

/**
 * 玩家信息控制类
 * 挂载到player预制体上
 */
@ccclass
export default class PlayerCtrl extends cc.Component {

    @property(cc.Label)
    idLable: cc.Label = null

    @property(cc.Label)
    nameLable: cc.Label = null

    @property(cc.Label)
    balanceLable: cc.Label = null

    @property(cc.Node)
    landImgNode: cc.Node = null    // 地主图标，显示该用户是否是地主

    private _user: IUserVo
    private _isLand: boolean = false

    // onLoad() {

    // }

    public init(user: IUserVo) {
        this._user = user
        this.idLable.string = `ID: ${user.id}`
        this.nameLable.string = `${user.name}`
        this.balanceLable.string = `$: ${user.balance}`
        this._isLand = false
    }

    /**
     * 设置是否是地主
     */
    public setLand(value: boolean) {
        this._isLand = value
        if (this._isLand) {
            this.landImgNode.active = true
        } else {
            this.landImgNode.active = false
        }
    }
}
